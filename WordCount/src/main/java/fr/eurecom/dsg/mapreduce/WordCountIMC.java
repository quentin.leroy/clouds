package fr.eurecom.dsg.mapreduce;

import java.io.IOException;
import java.util.HashMap;
import java.util.StringTokenizer;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

/**
 * Word Count example of MapReduce job. Given a plain text in input, this job
 * counts how many occurrences of each word there are in that text and writes
 * the result on HDFS.
 *
 */
public class WordCountIMC extends Configured implements Tool {

    private int numReducers;
    private Path inputPath;
    private Path outputDir;

    @Override
    public int run(String[] args) throws Exception {

        Configuration conf = this.getConf();
        Job job = null; // TODO: define new job instead of null using conf
        job = Job.getInstance(conf); //Done
        // TODO: set job input format
        job.setInputFormatClass(TextInputFormat.class);//Done
        // TODO: set map class and the map output key and value classes
        job.setMapperClass(WCIMCMapper.class);//Done
        job.setMapOutputKeyClass(Text.class);//Done
        job.setMapOutputValueClass(LongWritable.class);//Done
        // TODO: set reduce class and the reduce output key and value classes
        job.setReducerClass(WCIMCReducer.class);//Done
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(LongWritable.class);
        // TODO: set job output format
        job.setOutputFormatClass(TextOutputFormat.class);//Done
        // TODO: add the input file as job input (from HDFS) to the variable
        TextInputFormat.addInputPath(job, new Path(args[1]));//Done
        //       inputPath
        // TODO: set the output path for the job results (to HDFS) to the variable
        TextOutputFormat.setOutputPath(job, new Path(args[2]));//Done
        //       outputPath
        // TODO: set the number of reducers using variable numberReducers
        job.setNumReduceTasks(this.numReducers);//Done
        // TODO: set the jar class
        job.setJarByClass(WordCountIMC.class);


        return job.waitForCompletion(true) ? 0 : 1; // this will execute the job
    }

    public WordCountIMC (String[] args) {
        if (args.length != 3) {
            System.out.println("Usage: WordCountIMC <num_reducers> <input_path> <output_path>");
            System.exit(0);
        }
        this.numReducers = Integer.parseInt(args[0]);
        this.inputPath = new Path(args[1]);
        this.outputDir = new Path(args[2]);
    }

    public static void main(String args[]) throws Exception {
        int res = ToolRunner.run(new Configuration(), new WordCountIMC(args), args);
        System.exit(res);
    }
}

class WCIMCMapper extends Mapper<LongWritable, // TODO: change Object to input key
        // type
        Text, // TODO: change Object to input value type
        Text, // TODO: change Object to output key type
        LongWritable> { // TODO: change Object to output value type

    private final static LongWritable one = new LongWritable(1);
    private String word = new String();

    protected HashMap<String, Integer> hashMap;

    protected void setup(Context context) {
        this.hashMap = new HashMap<>();
    }

    @Override
    protected void map(LongWritable key, // TODO: change Object to input key type
                       Text value, // TODO: change Object to input value type
                       Context context) throws IOException, InterruptedException {

        StringTokenizer itr = new StringTokenizer(value.toString());
        while (itr.hasMoreTokens()) {
            word = itr.nextToken();
            Integer val = this.hashMap.get(word);
            if (val == null)
                this.hashMap.put(word, new Integer(1));
            else
                this.hashMap.put(word, val + 1);
        }


    }

    protected void cleanup(Context context) throws IOException, InterruptedException {
        for (String key : hashMap.keySet()) {
            context.write(new Text(key), new LongWritable(hashMap.get(key)));
        }
    }
}

class WCIMCReducer extends Reducer<Text, // TODO: change Object to input key
        // type
        LongWritable, // TODO: change Object to input value type
        Text, // TODO: change Object to output key type
        LongWritable> { // TODO: change Object to output value type

    private LongWritable result = new LongWritable();

    @Override
    protected void reduce(Text key, // TODO: change Object to input key type
                          Iterable<LongWritable> values, // TODO: change Object to input value type
                          Context context) throws IOException, InterruptedException {

        // TODO: implement the reduce method (use context.write to emit results)
        int sum = 0;
        for (LongWritable val : values) {
            sum += val.get();
        }
        result.set(sum);
        context.write(key, result);
    }
}